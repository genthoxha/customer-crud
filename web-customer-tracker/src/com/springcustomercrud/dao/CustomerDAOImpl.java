package com.springcustomercrud.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.springcustomercrud.entity.Customer;

@Repository
public class CustomerDAOImpl implements CustomerDAO{

	// need to inject the session factory
	@Autowired
	private SessionFactory sessionFactory;
	
	
	// there is no need for @Transactional because service is going to manage that , 
	@Override
	public List<Customer> getCustomers() {
		
		// get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		
		// create a query ... sort by last name
		Query<Customer> theQuery = currentSession.createQuery("from Customer order by lastName"
				
				,Customer.class);
		
		// execute query and get result list
		List<Customer> customers = theQuery.getResultList();
		
		// return the results
		return customers; 
		
	}


	@Override
	public void save(Customer customer) {
		// get current hibernate session 
		Session currentSession = sessionFactory.getCurrentSession();
		// save/update the customer ... finally
		currentSession.saveOrUpdate(customer);
	}


	@Override
	public Customer getCustomer(int theId) {
		// get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		
		// now retrieve/read from database using the primary key
		Customer theCustomer = currentSession.get(Customer.class, theId);
		
		return theCustomer;
		
	}


	@Override
	public void deletCustomerById(int theId) {
		// get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		
/*		// retrieve with no query
		Customer customerToDelete = currentSession.get(Customer.class, theId);
		
		// delete customer
		currentSession.delete(customerToDelete);*/
		
		// by query with HQL
		Query theQuery = currentSession.createQuery("delete from Customer where id=:customerId");
		theQuery.setParameter("customerId",theId);
		
		theQuery.executeUpdate();
		
		
	}


	@Override
	public List<Customer> searchCustomer(String theSearchName) {
		
		// get session
		Session currentSession = sessionFactory.getCurrentSession();
		
		Query theQuery = null;
		
		// only search by name if the name is not empty
		
		if(theSearchName != null && theSearchName.trim().length() > 0) {
			
			theQuery = currentSession.createQuery("from Customer where firstName like:theName or"
					+ " lastName like:theName",Customer.class);
			theQuery.setParameter("theName","%"+theSearchName.toLowerCase()+"%");
			
		}else {
			// the search name is empty just get all customers
			theQuery = currentSession.createQuery("from Customer",Customer.class);
		}
		
		// execute the query and get result list
		List<Customer> customers = theQuery.getResultList();
		System.out.println(customers);
		
		// return the results
		return customers;

	}
	
	

}
