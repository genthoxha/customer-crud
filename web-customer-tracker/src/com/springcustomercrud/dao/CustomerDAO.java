package com.springcustomercrud.dao;

import java.util.List;

import com.springcustomercrud.entity.Customer;

public interface CustomerDAO {

	public List<Customer> getCustomers();
	
	public void save(Customer customer);
	
	public Customer getCustomer(int theId);
	
	public void deletCustomerById(int theId);

	public List<Customer> searchCustomer(String theSearchName);
	
}
