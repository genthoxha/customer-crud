package com.springcustomercrud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springcustomercrud.dao.CustomerDAO;
import com.springcustomercrud.entity.Customer;

@Service
public class CustomerServiceImp implements CustomerService{

	// need to inject customer dao
	@Autowired
	CustomerDAO customerDAO;
	
	@Override
	@Transactional
	public List<Customer> getCustomers() {
		return customerDAO.getCustomers();
	}

	@Override
	@Transactional
	public void saveCustomer(Customer customer) {
		customerDAO.save(customer);
		
	}

	@Override
	@Transactional
	public Customer getCustomer(int theId) {
		return customerDAO.getCustomer(theId);
	}

	@Override
	@Transactional
	public void deleteCustomer(int theId) {
		 customerDAO.deletCustomerById(theId);
	}

	@Override
	@Transactional
	public List<Customer> searchCustomers(String theSearchName) {
		return  customerDAO.searchCustomer(theSearchName);// TODO Auto-generated method stub
	}
	
	

}
